package stack

import (
	"errors"
	"sync"
)

//==================================
//           L I F O
//----------------------------------

//var eMutex = sync.RWMutex{}

// Lifo type for create a new stack
type Lifo struct {
	value     []interface{}
	valueLock sync.RWMutex
}

// Push insert a new member instack
func (s *Lifo) Push(v interface{}) error {
	s.valueLock.Lock()
	defer s.valueLock.Unlock()

	s.value = append(s.value, v)

	// add element it's ok
	if s.value[len(s.value)-1] == v {
		return nil
	}
	return errors.New("it's not possible to add this element")
}

// Pop extract a last member from stack
func (s *Lifo) Pop() (interface{}, error) {
	s.valueLock.Lock()
	defer s.valueLock.Unlock()

	l := len(s.value)
	if l > 0 {
		sal := s.value[l-1]
		s.value = s.value[:l-1]
		return sal, nil
	}

	return 0, errors.New("there are no more elements")
}

// Len length of de stack
func (s *Lifo) Len() int {
	s.valueLock.RLock()
	defer s.valueLock.RUnlock()

	return len(s.value)
}

// Clear de stack
func (s *Lifo) Clear() error {
	s.valueLock.Lock()
	defer s.valueLock.Unlock()

	s.value = nil

	if len(s.value) == 0 {
		return nil
	}
	return errors.New("it's not possible to clear the elements")
}

//==================================
//           F I F O
//----------------------------------

// Fifo type for create a new stack
type Fifo struct {
	value     []interface{}
	valueLock sync.RWMutex
}

// Push insert a new member in queue
func (s *Fifo) Push(v interface{}) error {
	s.valueLock.Lock()
	defer s.valueLock.Unlock()

	s.value = append(s.value, v)
	// add element it's ok

	if s.value[len(s.value)-1] == v {
		return nil
	}
	return errors.New("it's not possible to add this element")
}

// Pop extract a first member from Queue
func (s *Fifo) Pop() (interface{}, error) {
	s.valueLock.Lock()
	defer s.valueLock.Unlock()

	if len(s.value) > 0 {
		sal := s.value[0]
		s.value = s.value[1:]
		return sal, nil
	}
	return 0, errors.New("there are no more elements")
}

// Len length of de Queue
func (s *Fifo) Len() int {
	s.valueLock.RLock()
	defer s.valueLock.RUnlock()

	return len(s.value)
}

// Clear de Queue
func (s *Fifo) Clear() error {
	s.valueLock.Lock()
	defer s.valueLock.Unlock()

	s.value = nil

	if len(s.value) == 0 {
		return nil
	}
	return errors.New("it's not possible to clear the elements")
}
