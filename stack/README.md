## Package stack

#### description


#### use
 > import "gitlab.com/norishen/utilpacks/stack"

#### index


type Lifo  
&emsp;&emsp;func (s *Lifo) Push(value T) error  
&emsp;&emsp;func (s *Lifo) Pop() (value T, error)  
&emsp;&emsp;func (s *Lifo) Clean() error  
&emsp;&emsp;func (s *Lifo) Len() int  

type Fifo  
&emsp;&emsp;func (s *Fifo) Push(value T) error  
&emsp;&emsp;func (s *Fifo) Pop() (value T, error)  
&emsp;&emsp;func (s *Fifo) Clean() error  
&emsp;&emsp;func (s *Fifo) Len() int  


#### examples

1. [Basic use of pacjage stack](/examples/stack/basic)
2. [Reverse Polish Notation (RPN) Calculator](/examples/stack/RPN_calc)

#### nexts changes
