package main

import (
	"fmt"

	"gitlab.com/norishen/utilpacks/stack"
)

func main() {

	fmt.Println("LIFO example program")

	// Define a new Lifo  stack
	var s1 stack.Lifo

	// push elements in stack
	fmt.Printf("Push: ")
	c := 10
	for x := 0; x < c; x++ {
		s1.Push(x)
		fmt.Printf("%v,", x)
	}
	fmt.Println()

	// pop elements from statck
	fmt.Printf("Pop: ")
	c = s1.Len()
	for x := 0; x < c; x++ {
		sal, _ := s1.Pop()
		fmt.Printf("%v,", sal)
	}
	fmt.Println()

	fmt.Printf("\n------------------------\n")
	fmt.Println()

	fmt.Println("FIFO example program")

	// Define a new Fifo stack
	var s2 stack.Fifo

	// push elements in stack
	fmt.Printf("Push: ")
	c = 10
	for x := 0; x < c; x++ {
		s2.Push(x)
		fmt.Printf("%v,", x)
	}
	fmt.Println()

	// pop elements from statck
	fmt.Printf("Pop: ")
	c = s2.Len()
	for x := 0; x < c; x++ {
		sal, _ := s2.Pop()
		fmt.Printf("%v,", sal)
	}
	fmt.Printf("\n------------------------\n")
}
