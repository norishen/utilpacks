# Reverse Polish Notation (RPN) Calculator

Simple program for demostrate de use of package stack

This text program, reads standar input, from keyboard, until enter is pressed. If the data is a number, it stores in the LIFO stack, (push) and if it's an operator, it takes the two last values in the stack, (pop) operates with them, and puts, (push) the result on the stack.

## Instructions
The controls are simple;

1. numbers from 0 to 9
2. operators +, - , * , /
3. "del" to delete the last entered value.
4. "ac" to delete all
5. "end" to finish program


