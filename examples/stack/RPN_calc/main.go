/*
Reverse Polish Notation (RPN) Calculator, using package stack

*/

package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/norishen/utilpacks/stack"
)

func main() {

	// LIFO stack
	var s1 stack.Lifo

	// VAR for keyboard input
	var dato string

	// VAR for actual operators in buffer
	display := []string{}

	// Main loop
	for {
		if len(display) != 0 {
			for _, v := range display {
				fmt.Printf("%s ", v)
			}

			// Control data prompt
			dato = ""
			fmt.Printf("> ")
		} else {
			fmt.Printf("0 > ")
		}
		// Read the keyboard until press Return
		fmt.Scanln(&dato)

		// select number or operation
		switch strings.ToUpper(dato) {

		// Addition
		case "+":
			if s1.Len() > 1 {
				v1, _ := s1.Pop()
				v2, _ := s1.Pop()
				tot := v2.(float64)
				tot += v1.(float64)

				s1.Push(tot)
				display = display[0 : s1.Len()-1]
				display = append(display, fmt.Sprintf("%v", tot))
			}

		// Substraction
		case "-":
			if s1.Len() > 1 {
				v1, _ := s1.Pop()
				v2, _ := s1.Pop()
				tot := v2.(float64)
				tot -= v1.(float64)

				s1.Push(tot)
				display = display[0 : s1.Len()-1]
				display = append(display, fmt.Sprintf("%v", tot))
			}

		// Multiplication
		case "*":
			if s1.Len() > 1 {
				v1, _ := s1.Pop()
				v2, _ := s1.Pop()
				tot := v2.(float64)
				tot *= v1.(float64)

				s1.Push(tot)
				display = display[0 : s1.Len()-1]
				display = append(display, fmt.Sprintf("%v", tot))
			}

		// Division
		case "/":
			if s1.Len() > 1 {
				v1, _ := s1.Pop()
				v2, _ := s1.Pop()
				tot := v2.(float64)
				tot /= v1.(float64)

				s1.Push(tot)
				display = display[0 : s1.Len()-1]
				display = append(display, fmt.Sprintf("%v", tot))
			}

		case "":
			continue

		case "DEL":
			if len(display) > 0 {
				display = display[:len(display)-1]

				_, err := s1.Pop()
				if err != nil {
					fmt.Println("Error deleting the last input")
				}
			}

		case "AC":
			display = nil
			err := s1.Clear()
			if err != nil {
				fmt.Println("Error deleting operations")
			}

		case "END":
			return

		default:
			i, err := strconv.ParseFloat(dato, 64)
			if err == nil && i != 0 {
				s1.Push(i)
				display = append(display, fmt.Sprintf("%v", dato))
			}
		}

	}
}
